
PURPOSE

For a web page broken up into sections this module dynamically provides a menu block with links to pages that belong to that section and a breadcrumb that drills down through sub items in that section.  This module is for sites where not all the pages in a section are included in the menu (so you are not able to use the active trail) but you still want to have a relevant breadcrumb and or menu of nodes in that section.

INSTALLATION

Install module as normal
A vocabulary is created called Page Structure.  Use this to create a hierarchial structure that represents your site structure.  Each top term in that vocabulary corresponds with a site section and each sub term in the vocabulary corresponds with a page that has sub-pages.  You may create a multi-level hierarchy
Go to admin/settings/webstruct and select the content types you wish to include in this system.  They will automatically get added to the vocabulary
Enable the block at admin/build/block

SETTINGS
There is a setting for Menu Depth.  The purpose of this is to control from what level you want the sub menu to begin, because you sometimes don't want higher level items to show.  However, on a breadcrumb you might want to show the higher level.  A clever person will create their Site Structure taxonomy in a manner which helps them create the ideal breadcrumb and sub menu system.

IMPLEMENTATION

For each node in the content types chosen for this module you will need to select the place in the structure to which it belongs by selecting the relevant item from the Page Structure taxonomy.  When viewing any node in a section, the block will show the full menu of pages within the section.

AN EXAMPLE

about 
  |
  -------------------------------------------------------
  |                                                      |
staff                                                Information
  |                                                      |
  ----------------                -------------------------------------------------       
  |               |               |                      |                         |
 web          marketing     mission statement         location                   history
 
For the above you might set up a Page Structure taxonomy that corresponds with the structure that you want to convey.  Assuming you want all the about pages to display in a menu when on any page in the about section, then you create a taxonomy as follows:

about <root>
 - about
   - staff
   - information
   
If you have a page with general about blurb then you select about <root>.  If not, it doesn't matter, the term name will display instead.  If you have a page with general blurb about staff then you select 'about' in the taxonomy. Similarly with 'information'.  Then any pages which belong to staff, you simply select 'staff'.  The <root> term is necessary to enable the correct breadcrumb to appear, and to ensure that, if you create an 'about' page then it displays in the correct place in the hierarchy.  If you do not need a general 'about' page, then you do not need the <root> term.
   
With the above structure your breadcrumb will then be of the type:

about > staff > web > [staff-name]

where [staff-name] is a node created in this section

To avoid having 'about' at the top of the submenu, you would then set Menu Depth to 1.

NOTE

This module uses the php similar_text() function to compare between a term name and its corresponding node.  Therefore it is important that for any parent term, your term name RESEMBLES the node which is assigned to it otherwise its child nodes will not appear in the right place on the menu.  In other words, the node name used above, called 'staff' must resemble the taxonomy term name which corresponds with 'staff'.  Similarity is done on a percentage basis and this percentage can be altered at admin/settings/webstruct.  This corresponds with the similarity we might expect from a user in assigning nodes to specific terms in the structure.  This applies only to nodes / terms which have child items.

FEATURES

The module is language sensitive
Items can be ordered within sub menus by altering them in the menu edit screen.  This order is preserved.  Note that any other changes made in the menu edit screen will be lost next time a node is updated or saved.

DEVELOPERS

The breadcrumb is available as a global variable $webstruct_bc
There is also a hook that can be used by other modules to modify the breadcrumb: hook_webstruct_breadcrumb($tree, $webstruct_bc)
